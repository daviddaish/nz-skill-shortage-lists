# ANZSCO continuity analysis

Included within this directory is an analysis of which occupations (as recorded
by their ANZSCO numbers) have occured over time in multiple editions of the
skill shortage lists.

Where the now outdated NZSCO code is used instead of the ANZSCO code, a
reasonable alternative ANZSCO number has been chosen using the included `12200
anzsco first edition to nzsco 1999 correspondence tables.xls` file.
