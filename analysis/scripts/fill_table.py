# Python 3.6.8

import sys
import pandas as pd

# Get the input file path, being a list of ANZSCO numbers to mark as being true.
anzsco_input_file = sys.argv[1]

# Get the existing ANZSCO table to serve as a template.
anzsco_table_path = "anzsco_table.csv"
anzsco_table = pd.read_csv(anzsco_table_path)

# Get the ANZSCO codes that are in the file
present_anzsco_codes = []
with open(anzsco_input_file, "r") as input_file:
    for line in input_file:
        num = line[:6]
        present_anzsco_codes.append(num)

# Mark the ANZSCO codes that are present truthy
truthy_indexes = []
for index, num in anzsco_table["ANZSCO number"].iteritems():

    if str(num) in present_anzsco_codes: 
        truthy_indexes.append(index)

truth_list = [True] * len(truthy_indexes)
truthy_series = pd.Series(data=truth_list, index=truthy_indexes)

anzsco_table["year"] = truthy_series

anzsco_table.to_csv('output.csv')
