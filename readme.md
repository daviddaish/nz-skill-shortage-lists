# NZ skill shortage lists

These are historic copies of the previous [New Zealand skill shortage
lists](https://www.immigration.govt.nz/employ-migrants/explore-your-options/before-you-start-hiring-migrants/skill-shortages),
which influence how easy it is to hire a migrant for a job in New Zealand.

This archive includes previous copies of the following lists:

* The Canterbury skill shortage list
* The Immediate skill shortage list
* The Construction and infrastructure skill shortage list
* The Long term skill shortage list

Some of these lists are no longer in use, and have been superseded by newer
lists.

For ease of use, the `analysis/anzsco_continuity` directory contains
spreadsheets summarising when which occupations appeared on which lists.

The `analysis/anzsco_continuity/compiled_skill_shortage_list.csv` file contains
a compilation of all the lists, resampled into regular, one month durations,
showing the true time skills have been listed on any of the lists.
